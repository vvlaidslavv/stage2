import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';

export async function renderFightersList() {
    try {
        App.rootElement.innerHTML = '';

        App.loadingElement.style.visibility = 'visible';

        const fighters = await fighterService.getFighters();
        const fightersElement = createFighters(fighters);

        App.rootElement.appendChild(fightersElement);
    } catch (error) {
        console.warn(error);
        App.rootElement.innerText = 'Failed to load data';
    } finally {
        App.loadingElement.style.visibility = 'hidden';
    }
}

class App {
    constructor() {
        this.startApp();
    }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
      await renderFightersList();
  }
}

export default App;
