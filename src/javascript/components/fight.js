import flatten  from 'lodash.flatten';

import { controls } from '../../constants/controls';
import { actionsDelayMs } from '../../constants/fight';

import { onChangeHandle } from '../helpers/dataHelper';

const availableKeyCodes = [...new Set(flatten(Object.values(controls)))];
const secondsTillNextCriticalComboAvailable = 10;

const defaultFighterData = {
    lastCriticalComboTimeStamp: 0,
    criticalComboKeys: [],
    fightHealthData: null,
};

export async function fight(firstFighter, secondFighter) {
    const { _id: firstFighterId } = firstFighter;
    const { _id: secondFighterId } = secondFighter;

    return new Promise((resolve) => {
        const fightersData = {
            [firstFighterId]: {
                ...defaultFighterData,
                ...firstFighter,
                criticalComboKeys: controls.PlayerOneCriticalHitCombination,
                fightHealthData:  {
                    barElement: document.getElementById('left-fighter-indicator'),
                    value: firstFighter.health.valueOf(),
                },
            },
            [secondFighterId]: {
                ...defaultFighterData,
                ...secondFighter,
                criticalComboKeys: controls.PlayerTwoCriticalHitCombination,
                fightHealthData: {
                    barElement: document.getElementById('right-fighter-indicator'),
                    value: secondFighter.health.valueOf(),
                },
            },
        };

        const finishFight = (winner) => {
            document.removeEventListener('keydown', keyDownListener);
            resolve(winner);
        };

        const initialKeyboardState = {
            buffer: [],
            lastKeyTimeStamp: 0,
        };
        let readyToHandleKeyboardStateChange = true;
        const handleKeyboardStateChange = (target) => {
            if (readyToHandleKeyboardStateChange) {
                readyToHandleKeyboardStateChange = false;

                // Timeout needed to  operate with fulfilled buffer
                setTimeout(() => {
                    readyToHandleKeyboardStateChange = true;

                    const { buffer } = target;
                    // Actions conditions
                    const shouldFirstFighterPerformCriticalCombo = shouldFighterPerformAction(
                        buffer,
                        [controls.PlayerOneCriticalHitCombination],
                        fightersData[firstFighterId].lastCriticalComboTimeStamp
                    );
                    const shouldSecondFighterPerformCriticalCombo = shouldFighterPerformAction(
                        buffer,
                        [controls.PlayerTwoCriticalHitCombination],
                        fightersData[secondFighterId].lastCriticalComboTimeStamp
                    );

                    const shouldFirstFighterBlock = shouldFighterPerformAction(
                        buffer,
                        [controls.PlayerOneBlock, controls.PlayerOneAttack]
                    );
                    const shouldSecondFighterBlock = shouldFighterPerformAction(
                        buffer,
                        [controls.PlayerTwoBlock, controls.PlayerTwoAttack]
                    );
                    const shouldFirstFighterAttack = shouldFighterPerformAction(
                        buffer,
                        [controls.PlayerOneAttack, controls.PlayerOneBlock]
                    );
                    const shouldSecondFighterAttack = shouldFighterPerformAction(
                        buffer,
                        [controls.PlayerTwoAttack, controls.PlayerTwoBlock]
                    );

                    // Actions
                    const performCriticalCombo = (fighterId, defenderId) => {
                        const attacker = fightersData[fighterId];
                        const defender = fightersData[defenderId];

                        const damage = getCriticalComboDamage(attacker);
                        const currentHealth = defender.fightHealthData.value - damage;

                        fightersData[fighterId].lastCriticalComboTimeStamp = Date.now();
                        defender.fightHealthData.barElement.style.width = `${currentHealth * 100 / defender.health}%`;
                        defender.fightHealthData.value = currentHealth;

                        if (currentHealth <= 0) {
                            finishFight(defender);
                        }
                    };
                    const performSimpleAttack = (fighterId, defenderId) => {
                        const attacker = fightersData[fighterId];
                        const defender = fightersData[defenderId];

                        const damage = getDamage(attacker, defender);
                        const currentHealth = defender.fightHealthData.value - damage;

                        defender.fightHealthData.barElement.style.width = `${currentHealth * 100 / defender.health}%`;
                        defender.fightHealthData.value = currentHealth;

                        if (currentHealth <= 0) {
                            finishFight(defender);
                        }
                    };

                    // Performing actions START
                    if (shouldFirstFighterPerformCriticalCombo) {
                        performCriticalCombo(firstFighterId, secondFighterId);
                    }
                    if (shouldSecondFighterPerformCriticalCombo) {
                        performCriticalCombo(secondFighterId, firstFighterId);
                    }

                    if (shouldFirstFighterAttack && !shouldFirstFighterPerformCriticalCombo && !shouldSecondFighterBlock) {
                        performSimpleAttack(firstFighterId, secondFighterId);
                    }
                    if (shouldSecondFighterAttack && !shouldSecondFighterPerformCriticalCombo && !shouldFirstFighterBlock) {
                        performSimpleAttack(secondFighterId, firstFighterId);
                    }
                }, actionsDelayMs);
            }
        };

        const state = onChangeHandle(initialKeyboardState, handleKeyboardStateChange);
        const keyDownListener = getKeyDownListener(state);

        document.addEventListener('keydown', keyDownListener);
    });
}

function getCriticalComboDamage({ attack }) {
    return 2 * attack;
}

export function getDamage(attacker, defender) {
    const damage = getHitPower(attacker) - getBlockPower(defender);
    return damage > 0 ? damage : 0;
}

export function getHitPower({ attack }) {
    const criticalHitChance = getFightEventChance();
    return attack * criticalHitChance;
}

export function getBlockPower({ defense }) {
    const dodgeChance = getFightEventChance();
    return defense * dodgeChance;
}

function getFightEventChance() {
    return Math.random() * (2 - 1) + 1;
}

function shouldFighterPerformAction(buffer, actionsKeyCodesArr, lastCriticalComboTimestamp = 0) {
    const [desirableAction, interruptingAction] = actionsKeyCodesArr;

    if (Array.isArray(desirableAction)) {
        const bufferHasActionKeysCodes = desirableAction.every(keyCode => buffer.includes(keyCode));
        if (bufferHasActionKeysCodes) {
            return Math.floor((Date.now() - lastCriticalComboTimestamp) / 1000) > secondsTillNextCriticalComboAvailable;
        }
        return false;
    }

    if (buffer.includes(desirableAction)) {
        if (buffer.includes(interruptingAction)) {
            const desirableCodeIndex = buffer.findIndex(keyCode => keyCode === desirableAction);
            const interruptingCodeIndex = buffer.findIndex(keyCode => keyCode === interruptingAction);
            return desirableCodeIndex < interruptingCodeIndex;
        }

        return true;
    }

    return false;
}

function getKeyDownListener(state) {
    return ({ code: currentCode, timeStamp: currentTimeStamp }) => {
        const isAvailableKeyCode = !!availableKeyCodes.some(keyCode => keyCode === currentCode);
        if (!isAvailableKeyCode) {
            return;
        }

        let buffer = [];

        if (currentTimeStamp - state.lastKeyTimeStamp > actionsDelayMs) {
            buffer = [currentCode];
        } else {
            buffer = [...state.buffer, currentCode];
        }

        state.buffer = buffer;
        state.lastKeyTimeStamp = currentTimeStamp;
    };
}
