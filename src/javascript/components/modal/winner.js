import { createElement } from '../../helpers/domHelper';
import { renderFightersList } from '../../app';

import { showModal } from './modal';

export function showWinnerModal(fighter) {
    const title = `${fighter.name} WON`;

    const backToFightersList = () => renderFightersList();

    const textContainer = createElement({ tagName: 'div', className: 'ko-container' });
    const letterK = createElement({ tagName: 'span', className: 'ko-letter' });
    letterK.innerText = 'K.';
    const letterO = createElement({ tagName: 'span', className: 'ko-letter', attributes: { innerText: 'O' } });
    letterO.innerText = 'O.';

    const controls = createElement({ tagName: 'div', className: 'modal-controls' });
    const backButton = createElement({ tagName: 'button', className: 'modal-control' });
    backButton.addEventListener('click', backToFightersList, false);
    backButton.innerText = 'Back to Fighters List';

    const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });

    textContainer.append(letterK, letterO);
    controls.append(backButton);
    bodyElement.append(textContainer, controls);

    showModal({
        title,
        bodyElement,
        onClose: backToFightersList,
    });
}
