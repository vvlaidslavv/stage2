import { createElement } from '../helpers/domHelper';

// TODO: Display splash for fighter who haven't been chosen yet
export function createFighterPreview(fighter = {}, position) {
    const {
        _id, // eslint-disable-line
        source,
        name,
        ...fighterPreviewProperties
    } = fighter;

    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });
    const fighterImg = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes: {
            src: source,
            title: name,
            alt: `${position === 'right' ? 'first' : 'second'} fighter preview`,
        }
    });
    const fighterInfoContainer = createElement({
        tagName: 'ul',
        className: 'fighter-preview___info-container info-box',
    });
    const fighterElements = Object.keys(fighterPreviewProperties)
        .map(fighterPropertyKey => createFighterInfoElement({
            [fighterPropertyKey]: fighter[fighterPropertyKey],
        }));

    fighterInfoContainer.append(...fighterElements);
    fighterElement.append(fighterImg, fighterInfoContainer);

    return fighterElement;
}

function createFighterInfoElement(propertyInfo) {
    const propertyName = Object.keys(propertyInfo)[0];

    const fighterInfoElement = createElement({
        tagName: 'li',
        className: `fighter-preview___info-element ${propertyName} `,
    });

    const fighterInfoElementTitle = createElement({
        tagName: 'span',
        className: 'fighter-preview___info-element-title',
    });
    const fighterInfoElementValue = createElement({
        tagName: 'span',
        className: 'fighter-preview___info-element-value',
    });

    fighterInfoElementTitle.append(`${propertyName}: `);
    fighterInfoElementValue.append(propertyInfo[propertyName]);
    fighterInfoElement.append(fighterInfoElementTitle, fighterInfoElementValue);

    return fighterInfoElement;
}

export function createFighterImage(fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

    return imgElement;
}
